import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentCampaing: null,
    chatEmails: new Map(),
    editingProds: "hoal"
  },
  mutations: {
    setNewEmailToList(state, email) {
      //Datos vista store
      state.chatEmails.set(email, true);
    },
    deleteEmailFromList(state, email) {
      state.chatEmails.delete(email);
    },
    async editingCampaing(state, campaing) {
      state.currentCampaing = campaing;
      state.editingProds = true;
    },
    async finishEditingCampaing(state, campaing) {
      state.currentCampaing = null;
      state.editingProds = false;
    }
  },
  actions: {},
  modules: {},
  getters: {
    emailsList: state => state.chatEmails,
    prueba: state => state.prueba
  }
});
