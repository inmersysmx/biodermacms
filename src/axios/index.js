import axios from "axios";
import dotenv from "dotenv";
dotenv.config();

export const axiosInstance = axios.create({
  //baseURL: 'https://bioderma-dev.herokuapp.com/',
  //  baseURL: 'https://bioderma-api-inmersys.herokuapp.com/',
  baseURL: "http://localhost:3000/"
  // baseURL: "http://localhost:3000/"
  // headers: {
  //     Authorization: {
  //         toString() {
  //             return `Bearer ${localStorage.getItem('token')}`
  //         }
  //     }
  // }
});
